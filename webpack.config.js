const webpack = require('webpack')
var  ExtractTextPlugin = require('extract-text-webpack-plugin');
var VueLoaderPlugin = require('vue-loader/lib/plugin')
var path = require('path');
// require("./mapview")
var config = {
    entry: {

        main: './main'
    },
    output: {
        path: path.join(__dirname, './static/dist'),
        publicPath: '/dist',
        filename: '[name].js',
        chunkFilename:'[name].chunk.js'
    }
   ,
   devServer: {
    historyApiFallback: true,
    contentBase: "./",
    quiet: false, //控制台中不输出打包的信息
    noInfo: false,
    hot: true, //开启热点
    inline: true, //开启页面自动刷新
    lazy: false, //不启动懒加载
    progress: true, //显示打包的进度
    watchOptions: {
        aggregateTimeout: 300
    },
    port: '8084', //设置端口号
    //其实很简单的，只要配置这个参数就可以了
    proxy: {
        '/geoserver/*': {
            target: 'http://localhost:8080/geoserver/HJ/wms',
            secure: false
        }
    }
},
    module: {
        rules: [
            // {
            //     test:/\.(gif|jpg|png|woff|svg|eot|ttf)\??.*$/,
            //     loader:'url-loader?limit=1024'
            // },
            {
                test:/\.(gif|jpg|png|woff|svg|eot|ttf)\??.*$/,
                loader:'url-loader',
                options: {
                    limit: 102400,
                    name: 'img/[name].[hash:7].[ext]',
                    publicPath:"/static/"
                }
            },
            {
            test: /\.vue$/,
            loader: 'vue-loader',
            options: {
                loaders: {
                    css: ExtractTextPlugin.extract({
                        use: 'css-loader',
                        fallback: 'vue-style-loader'
                    })
                }
            }
        },
        {
            test: /\.js$/,
            exclude: /(node_modules|bower_components)/,
            use: {
              loader: 'babel-loader',
              options: {
                presets: ['@babel/preset-env'],
                plugins: ['@babel/transform-runtime']
              }
            }
          }, {
            test: /\.css$/,
            use: ExtractTextPlugin.extract({
                use: 'css-loader',
                fallback: 'style-loader'
            })
        }

        ]
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new ExtractTextPlugin(
            {
                filename:'[name].css',
                allChunks:true
            }),
        new VueLoaderPlugin()
    ]
}

module.exports = config