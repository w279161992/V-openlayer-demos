const maprouters = [
{
path:'/accessible',
name:'accessible',
component:(resolve) => require(['./views/accessible.vue'],resolve),
},
{
path:'/animation',
name:'animation',
component:(resolve) => require(['./views/animation.vue'],resolve),
},
{
path:'/arcgis-image',
name:'arcgis-image',
component:(resolve) => require(['./views/arcgis-image.vue'],resolve),
},
{
path:'/arcgis-tiled',
name:'arcgis-tiled',
component:(resolve) => require(['./views/arcgis-tiled.vue'],resolve),
},
{
path:'/attributions',
name:'attributions',
component:(resolve) => require(['./views/attributions.vue'],resolve),
},
{
path:'/bing-maps',
name:'bing-maps',
component:(resolve) => require(['./views/bing-maps.vue'],resolve),
},
{
path:'/blend-modes',
name:'blend-modes',
component:(resolve) => require(['./views/blend-modes.vue'],resolve),
},
{
path:'/box-selection',
name:'box-selection',
component:(resolve) => require(['./views/box-selection.vue'],resolve),
},
{
path:'/button-title',
name:'button-title',
component:(resolve) => require(['./views/button-title.vue'],resolve),
},
{
path:'/canvas-gradient-pattern',
name:'canvas-gradient-pattern',
component:(resolve) => require(['./views/canvas-gradient-pattern.vue'],resolve),
},
{
path:'/canvas-tiles',
name:'canvas-tiles',
component:(resolve) => require(['./views/canvas-tiles.vue'],resolve),
},
{
path:'/cartodb',
name:'cartodb',
component:(resolve) => require(['./views/cartodb.vue'],resolve),
},
{
path:'/center',
name:'center',
component:(resolve) => require(['./views/center.vue'],resolve),
},
{
path:'/chaikin',
name:'chaikin',
component:(resolve) => require(['./views/chaikin.vue'],resolve),
},
{
path:'/cluster',
name:'cluster',
component:(resolve) => require(['./views/cluster.vue'],resolve),
},
{
path:'/color-manipulation',
name:'color-manipulation',
component:(resolve) => require(['./views/color-manipulation.vue'],resolve),
},
{
path:'/custom-controls',
name:'custom-controls',
component:(resolve) => require(['./views/custom-controls.vue'],resolve),
},
{
path:'/custom-interactions',
name:'custom-interactions',
component:(resolve) => require(['./views/custom-interactions.vue'],resolve),
},
{
path:'/d3',
name:'d3',
component:(resolve) => require(['./views/d3.vue'],resolve),
},
{
path:'/dash',
name:'dash',
component:(resolve) => require(['./views/dash.vue'],resolve),
},
{
path:'/device-orientation',
name:'device-orientation',
component:(resolve) => require(['./views/device-orientation.vue'],resolve),
},
{
path:'/drag-and-drop-image-vector',
name:'drag-and-drop-image-vector',
component:(resolve) => require(['./views/drag-and-drop-image-vector.vue'],resolve),
},
{
path:'/drag-and-drop',
name:'drag-and-drop',
component:(resolve) => require(['./views/drag-and-drop.vue'],resolve),
},
{
path:'/drag-rotate-and-zoom',
name:'drag-rotate-and-zoom',
component:(resolve) => require(['./views/drag-rotate-and-zoom.vue'],resolve),
},
{
path:'/draw-and-modify-features',
name:'draw-and-modify-features',
component:(resolve) => require(['./views/draw-and-modify-features.vue'],resolve),
},
{
path:'/draw-features',
name:'draw-features',
component:(resolve) => require(['./views/draw-features.vue'],resolve),
},
{
path:'/draw-freehand',
name:'draw-freehand',
component:(resolve) => require(['./views/draw-freehand.vue'],resolve),
},
{
path:'/draw-shapes',
name:'draw-shapes',
component:(resolve) => require(['./views/draw-shapes.vue'],resolve),
},
{
path:'/dynamic-data',
name:'dynamic-data',
component:(resolve) => require(['./views/dynamic-data.vue'],resolve),
},
{
path:'/earthquake-clusters',
name:'earthquake-clusters',
component:(resolve) => require(['./views/earthquake-clusters.vue'],resolve),
},
{
path:'/earthquake-custom-symbol',
name:'earthquake-custom-symbol',
component:(resolve) => require(['./views/earthquake-custom-symbol.vue'],resolve),
},
{
path:'/epsg-4326',
name:'epsg-4326',
component:(resolve) => require(['./views/epsg-4326.vue'],resolve),
},
{
path:'/export-map',
name:'export-map',
component:(resolve) => require(['./views/export-map.vue'],resolve),
},
{
path:'/export-pdf',
name:'export-pdf',
component:(resolve) => require(['./views/export-pdf.vue'],resolve),
},
{
path:'/extent-interaction',
name:'extent-interaction',
component:(resolve) => require(['./views/extent-interaction.vue'],resolve),
},
{
path:'/feature-animation',
name:'feature-animation',
component:(resolve) => require(['./views/feature-animation.vue'],resolve),
},
{
path:'/feature-move-animation',
name:'feature-move-animation',
component:(resolve) => require(['./views/feature-move-animation.vue'],resolve),
},
{
path:'/flight-animation',
name:'flight-animation',
component:(resolve) => require(['./views/flight-animation.vue'],resolve),
},
{
path:'/fractal',
name:'fractal',
component:(resolve) => require(['./views/fractal.vue'],resolve),
},
{
path:'/full-screen-drag-rotate-and-zoom',
name:'full-screen-drag-rotate-and-zoom',
component:(resolve) => require(['./views/full-screen-drag-rotate-and-zoom.vue'],resolve),
},
{
path:'/full-screen-source',
name:'full-screen-source',
component:(resolve) => require(['./views/full-screen-source.vue'],resolve),
},
{
path:'/full-screen',
name:'full-screen',
component:(resolve) => require(['./views/full-screen.vue'],resolve),
},
{
path:'/geojson-vt',
name:'geojson-vt',
component:(resolve) => require(['./views/geojson-vt.vue'],resolve),
},
{
path:'/geojson',
name:'geojson',
component:(resolve) => require(['./views/geojson.vue'],resolve),
},
{
path:'/geolocation-orientation',
name:'geolocation-orientation',
component:(resolve) => require(['./views/geolocation-orientation.vue'],resolve),
},
{
path:'/geolocation',
name:'geolocation',
component:(resolve) => require(['./views/geolocation.vue'],resolve),
},
{
path:'/getfeatureinfo-image',
name:'getfeatureinfo-image',
component:(resolve) => require(['./views/getfeatureinfo-image.vue'],resolve),
},
{
path:'/getfeatureinfo-layers',
name:'getfeatureinfo-layers',
component:(resolve) => require(['./views/getfeatureinfo-layers.vue'],resolve),
},
{
path:'/getfeatureinfo-tile',
name:'getfeatureinfo-tile',
component:(resolve) => require(['./views/getfeatureinfo-tile.vue'],resolve),
},
{
path:'/gpx',
name:'gpx',
component:(resolve) => require(['./views/gpx.vue'],resolve),
},
{
path:'/graticule',
name:'graticule',
component:(resolve) => require(['./views/graticule.vue'],resolve),
},
{
path:'/heatmap-earthquakes',
name:'heatmap-earthquakes',
component:(resolve) => require(['./views/heatmap-earthquakes.vue'],resolve),
},
{
path:'/here-maps',
name:'here-maps',
component:(resolve) => require(['./views/here-maps.vue'],resolve),
},
{
path:'/hit-tolerance',
name:'hit-tolerance',
component:(resolve) => require(['./views/hit-tolerance.vue'],resolve),
},
{
path:'/icon-color',
name:'icon-color',
component:(resolve) => require(['./views/icon-color.vue'],resolve),
},
{
path:'/icon-negative',
name:'icon-negative',
component:(resolve) => require(['./views/icon-negative.vue'],resolve),
},
{
path:'/icon-sprite-webgl',
name:'icon-sprite-webgl',
component:(resolve) => require(['./views/icon-sprite-webgl.vue'],resolve),
},
{
path:'/icon',
name:'icon',
component:(resolve) => require(['./views/icon.vue'],resolve),
},
{
path:'/igc',
name:'igc',
component:(resolve) => require(['./views/igc.vue'],resolve),
},
{
path:'/image-filter',
name:'image-filter',
component:(resolve) => require(['./views/image-filter.vue'],resolve),
},
{
path:'/image-load-events',
name:'image-load-events',
component:(resolve) => require(['./views/image-load-events.vue'],resolve),
},
{
path:'/image-vector-layer',
name:'image-vector-layer',
component:(resolve) => require(['./views/image-vector-layer.vue'],resolve),
},
{
path:'/interaction-options',
name:'interaction-options',
component:(resolve) => require(['./views/interaction-options.vue'],resolve),
},
{
path:'/jsts',
name:'jsts',
component:(resolve) => require(['./views/jsts.vue'],resolve),
},
{
path:'/kml-earthquakes',
name:'kml-earthquakes',
component:(resolve) => require(['./views/kml-earthquakes.vue'],resolve),
},
{
path:'/kml-timezones',
name:'kml-timezones',
component:(resolve) => require(['./views/kml-timezones.vue'],resolve),
},
{
path:'/KML',
name:'KML',
component:(resolve) => require(['./views/KML.vue'],resolve),
},
{
path:'/layer-clipping-webgl',
name:'layer-clipping-webgl',
component:(resolve) => require(['./views/layer-clipping-webgl.vue'],resolve),
},
{
path:'/layer-clipping',
name:'layer-clipping',
component:(resolve) => require(['./views/layer-clipping.vue'],resolve),
},
{
path:'/layer-extent',
name:'layer-extent',
component:(resolve) => require(['./views/layer-extent.vue'],resolve),
},
{
path:'/layer-group',
name:'layer-group',
component:(resolve) => require(['./views/layer-group.vue'],resolve),
},
{
path:'/layer-spy',
name:'layer-spy',
component:(resolve) => require(['./views/layer-spy.vue'],resolve),
},
{
path:'/layer-swipe',
name:'layer-swipe',
component:(resolve) => require(['./views/layer-swipe.vue'],resolve),
},
{
path:'/layer-z-index',
name:'layer-z-index',
component:(resolve) => require(['./views/layer-z-index.vue'],resolve),
},
{
path:'/lazy-source',
name:'lazy-source',
component:(resolve) => require(['./views/lazy-source.vue'],resolve),
},
{
path:'/line-arrows',
name:'line-arrows',
component:(resolve) => require(['./views/line-arrows.vue'],resolve),
},
{
path:'/localized-openstreetmap',
name:'localized-openstreetmap',
component:(resolve) => require(['./views/localized-openstreetmap.vue'],resolve),
},
{
path:'/magnify',
name:'magnify',
component:(resolve) => require(['./views/magnify.vue'],resolve),
},
{
path:'/Map export',
name:'Map export',
component:(resolve) => require(['./views/Map export.vue'],resolve),
},
{
path:'/Map network',
name:'Map network',
component:(resolve) => require(['./views/Map network.vue'],resolve),
},
{
path:'/mapbox-vector-tiles-advanced',
name:'mapbox-vector-tiles-advanced',
component:(resolve) => require(['./views/mapbox-vector-tiles-advanced.vue'],resolve),
},
{
path:'/mapbox-vector-tiles',
name:'mapbox-vector-tiles',
component:(resolve) => require(['./views/mapbox-vector-tiles.vue'],resolve),
},
{
path:'/mapguide-untiled',
name:'mapguide-untiled',
component:(resolve) => require(['./views/mapguide-untiled.vue'],resolve),
},
{
path:'/measure',
name:'measure',
component:(resolve) => require(['./views/measure.vue'],resolve),
},
{
path:'/min-max-resolution',
name:'min-max-resolution',
component:(resolve) => require(['./views/min-max-resolution.vue'],resolve),
},
{
path:'/min-zoom',
name:'min-zoom',
component:(resolve) => require(['./views/min-zoom.vue'],resolve),
},
{
path:'/mobile-full-screen',
name:'mobile-full-screen',
component:(resolve) => require(['./views/mobile-full-screen.vue'],resolve),
},
{
path:'/Modification function',
name:'Modification function',
component:(resolve) => require(['./views/Modification function.vue'],resolve),
},
{
path:'/modify-features',
name:'modify-features',
component:(resolve) => require(['./views/modify-features.vue'],resolve),
},
{
path:'/modify-test',
name:'modify-test',
component:(resolve) => require(['./views/modify-test.vue'],resolve),
},
{
path:'/mouse-position',
name:'mouse-position',
component:(resolve) => require(['./views/mouse-position.vue'],resolve),
},
{
path:'/moveend',
name:'moveend',
component:(resolve) => require(['./views/moveend.vue'],resolve),
},
{
path:'/navigation-controls',
name:'navigation-controls',
component:(resolve) => require(['./views/navigation-controls.vue'],resolve),
},
{
path:'/osm-vector-tiles',
name:'osm-vector-tiles',
component:(resolve) => require(['./views/osm-vector-tiles.vue'],resolve),
},
{
path:'/overlay',
name:'overlay',
component:(resolve) => require(['./views/overlay.vue'],resolve),
},
{
path:'/overviewmap-custom',
name:'overviewmap-custom',
component:(resolve) => require(['./views/overviewmap-custom.vue'],resolve),
},
{
path:'/overviewmap',
name:'overviewmap',
component:(resolve) => require(['./views/overviewmap.vue'],resolve),
},
{
path:'/permalink',
name:'permalink',
component:(resolve) => require(['./views/permalink.vue'],resolve),
},
{
path:'/pinch-zoom',
name:'pinch-zoom',
component:(resolve) => require(['./views/pinch-zoom.vue'],resolve),
},
{
path:'/polygon-styles',
name:'polygon-styles',
component:(resolve) => require(['./views/polygon-styles.vue'],resolve),
},
{
path:'/popup',
name:'popup',
component:(resolve) => require(['./views/popup.vue'],resolve),
},
{
path:'/preload',
name:'preload',
component:(resolve) => require(['./views/preload.vue'],resolve),
},
{
path:'/raster',
name:'raster',
component:(resolve) => require(['./views/raster.vue'],resolve),
},
{
path:'/region-growing',
name:'region-growing',
component:(resolve) => require(['./views/region-growing.vue'],resolve),
},
{
path:'/regularshape',
name:'regularshape',
component:(resolve) => require(['./views/regularshape.vue'],resolve),
},
{
path:'/render-geometry',
name:'render-geometry',
component:(resolve) => require(['./views/render-geometry.vue'],resolve),
},
{
path:'/reprojection-by-code',
name:'reprojection-by-code',
component:(resolve) => require(['./views/reprojection-by-code.vue'],resolve),
},
{
path:'/reprojection-image',
name:'reprojection-image',
component:(resolve) => require(['./views/reprojection-image.vue'],resolve),
},
{
path:'/reprojection-wgs84',
name:'reprojection-wgs84',
component:(resolve) => require(['./views/reprojection-wgs84.vue'],resolve),
},
{
path:'/reprojection',
name:'reprojection',
component:(resolve) => require(['./views/reprojection.vue'],resolve),
},
{
path:'/reusable-source',
name:'reusable-source',
component:(resolve) => require(['./views/reusable-source.vue'],resolve),
},
{
path:'/rotation',
name:'rotation',
component:(resolve) => require(['./views/rotation.vue'],resolve),
},
{
path:'/scale-line',
name:'scale-line',
component:(resolve) => require(['./views/scale-line.vue'],resolve),
},
{
path:'/scaleline-indiana-east',
name:'scaleline-indiana-east',
component:(resolve) => require(['./views/scaleline-indiana-east.vue'],resolve),
},
{
path:'/sea-level',
name:'sea-level',
component:(resolve) => require(['./views/sea-level.vue'],resolve),
},
{
path:'/select-features',
name:'select-features',
component:(resolve) => require(['./views/select-features.vue'],resolve),
},
{
path:'/semi-transparent-layer',
name:'semi-transparent-layer',
component:(resolve) => require(['./views/semi-transparent-layer.vue'],resolve),
},
{
path:'/shaded-relief',
name:'shaded-relief',
component:(resolve) => require(['./views/shaded-relief.vue'],resolve),
},
{
path:'/side-by-side',
name:'side-by-side',
component:(resolve) => require(['./views/side-by-side.vue'],resolve),
},
{
path:'/simple',
name:'simple',
component:(resolve) => require(['./views/simple.vue'],resolve),
},
{
path:'/snap',
name:'snap',
component:(resolve) => require(['./views/snap.vue'],resolve),
},
{
path:'/sphere-mollweide',
name:'sphere-mollweide',
component:(resolve) => require(['./views/sphere-mollweide.vue'],resolve),
},
{
path:'/stamen',
name:'stamen',
component:(resolve) => require(['./views/stamen.vue'],resolve),
},
{
path:'/static-image',
name:'static-image',
component:(resolve) => require(['./views/static-image.vue'],resolve),
},
{
path:'/street-labels',
name:'street-labels',
component:(resolve) => require(['./views/street-labels.vue'],resolve),
},
{
path:'/symbol-atlas-webgl',
name:'symbol-atlas-webgl',
component:(resolve) => require(['./views/symbol-atlas-webgl.vue'],resolve),
},
{
path:'/synthetic-lines',
name:'synthetic-lines',
component:(resolve) => require(['./views/synthetic-lines.vue'],resolve),
},
{
path:'/synthetic-points',
name:'synthetic-points',
component:(resolve) => require(['./views/synthetic-points.vue'],resolve),
},
{
path:'/teleport',
name:'teleport',
component:(resolve) => require(['./views/teleport.vue'],resolve),
},
{
path:'/tile-load-events',
name:'tile-load-events',
component:(resolve) => require(['./views/tile-load-events.vue'],resolve),
},
{
path:'/tile-transitions',
name:'tile-transitions',
component:(resolve) => require(['./views/tile-transitions.vue'],resolve),
},
{
path:'/tilejson',
name:'tilejson',
component:(resolve) => require(['./views/tilejson.vue'],resolve),
},
{
path:'/tissot',
name:'tissot',
component:(resolve) => require(['./views/tissot.vue'],resolve),
},
{
path:'/topojson',
name:'topojson',
component:(resolve) => require(['./views/topojson.vue'],resolve),
},
{
path:'/topolis',
name:'topolis',
component:(resolve) => require(['./views/topolis.vue'],resolve),
},
{
path:'/translate-features',
name:'translate-features',
component:(resolve) => require(['./views/translate-features.vue'],resolve),
},
{
path:'/turf',
name:'turf',
component:(resolve) => require(['./views/turf.vue'],resolve),
},
{
path:'/utfgrid',
name:'utfgrid',
component:(resolve) => require(['./views/utfgrid.vue'],resolve),
},
{
path:'/vector-esri-edit',
name:'vector-esri-edit',
component:(resolve) => require(['./views/vector-esri-edit.vue'],resolve),
},
{
path:'/vector-esri',
name:'vector-esri',
component:(resolve) => require(['./views/vector-esri.vue'],resolve),
},
{
path:'/vector-label-decluttering',
name:'vector-label-decluttering',
component:(resolve) => require(['./views/vector-label-decluttering.vue'],resolve),
},
{
path:'/vector-labels',
name:'vector-labels',
component:(resolve) => require(['./views/vector-labels.vue'],resolve),
},
{
path:'/vector-layer',
name:'vector-layer',
component:(resolve) => require(['./views/vector-layer.vue'],resolve),
},
{
path:'/vector-osm',
name:'vector-osm',
component:(resolve) => require(['./views/vector-osm.vue'],resolve),
},
{
path:'/vector-tile-info',
name:'vector-tile-info',
component:(resolve) => require(['./views/vector-tile-info.vue'],resolve),
},
{
path:'/vector-tile-selection',
name:'vector-tile-selection',
component:(resolve) => require(['./views/vector-tile-selection.vue'],resolve),
},
{
path:'/vector-wfs-getfeature',
name:'vector-wfs-getfeature',
component:(resolve) => require(['./views/vector-wfs-getfeature.vue'],resolve),
},
{
path:'/vector-wfs',
name:'vector-wfs',
component:(resolve) => require(['./views/vector-wfs.vue'],resolve),
},
{
path:'/wkt',
name:'wkt',
component:(resolve) => require(['./views/wkt.vue'],resolve),
},
{
path:'/wms-capabilities',
name:'wms-capabilities',
component:(resolve) => require(['./views/wms-capabilities.vue'],resolve),
},
{
path:'/wms-custom-proj',
name:'wms-custom-proj',
component:(resolve) => require(['./views/wms-custom-proj.vue'],resolve),
},
{
path:'/wms-custom-tilegrid-512x256',
name:'wms-custom-tilegrid-512x256',
component:(resolve) => require(['./views/wms-custom-tilegrid-512x256.vue'],resolve),
},
{
path:'/wms-image-custom-proj',
name:'wms-image-custom-proj',
component:(resolve) => require(['./views/wms-image-custom-proj.vue'],resolve),
},
{
path:'/wms-image',
name:'wms-image',
component:(resolve) => require(['./views/wms-image.vue'],resolve),
},
{
path:'/wms-no-proj',
name:'wms-no-proj',
component:(resolve) => require(['./views/wms-no-proj.vue'],resolve),
},
{
path:'/wms-tiled-wrap-180',
name:'wms-tiled-wrap-180',
component:(resolve) => require(['./views/wms-tiled-wrap-180.vue'],resolve),
},
{
path:'/wms-tiled',
name:'wms-tiled',
component:(resolve) => require(['./views/wms-tiled.vue'],resolve),
},
{
path:'/wms-time',
name:'wms-time',
component:(resolve) => require(['./views/wms-time.vue'],resolve),
},
{
path:'/wmts-capabilities',
name:'wmts-capabilities',
component:(resolve) => require(['./views/wmts-capabilities.vue'],resolve),
},
{
path:'/wmts-dimensions',
name:'wmts-dimensions',
component:(resolve) => require(['./views/wmts-dimensions.vue'],resolve),
},
{
path:'/wmts-hidpi',
name:'wmts-hidpi',
component:(resolve) => require(['./views/wmts-hidpi.vue'],resolve),
},
{
path:'/wmts-ign',
name:'wmts-ign',
component:(resolve) => require(['./views/wmts-ign.vue'],resolve),
},
{
path:'/wmts-layer-from-capabilities',
name:'wmts-layer-from-capabilities',
component:(resolve) => require(['./views/wmts-layer-from-capabilities.vue'],resolve),
},
{
path:'/wmts',
name:'wmts',
component:(resolve) => require(['./views/wmts.vue'],resolve),
},
{
path:'/xyz-esri-4326-512',
name:'xyz-esri-4326-512',
component:(resolve) => require(['./views/xyz-esri-4326-512.vue'],resolve),
},
{
path:'/xyz-esri',
name:'xyz-esri',
component:(resolve) => require(['./views/xyz-esri.vue'],resolve),
},
{
path:'/xyz-retina',
name:'xyz-retina',
component:(resolve) => require(['./views/xyz-retina.vue'],resolve),
},
{
path:'/xyz',
name:'xyz',
component:(resolve) => require(['./views/xyz.vue'],resolve),
},
{
path:'/zoom-constrained',
name:'zoom-constrained',
component:(resolve) => require(['./views/zoom-constrained.vue'],resolve),
},
{
path:'/zoomify',
name:'zoomify',
component:(resolve) => require(['./views/zoomify.vue'],resolve),
},
{
path:'/zoomslider',
name:'zoomslider',
component:(resolve) => require(['./views/zoomslider.vue'],resolve),
},
{
path:'*',
redirect:'/directory',
},
{
path:'/',
redirect:'/directory',
}
]
export default maprouters