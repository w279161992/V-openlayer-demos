import Vue from  'vue';
import VueRouter from 'vue-router';
import vuex from 'vuex';
import App from './app.vue';
import 'element-ui/lib/theme-chalk/index.css';
import ElementUi from 'element-ui';
import Directory from './directory.vue';
import Inspect from './inspect.vue';
import { resolve } from 'path';
import History from 'connect-history-api-fallback';
import maprouters from './route.js'
// var history = require('connect-history-api-fallback');

Vue.use(VueRouter);
Vue.use(vuex);
Vue.use(ElementUi);
// Vue.use(History);
// const history = new History({});
// history({
//     rewrites: [
//       { from: /\//, to: '/index'},
//       { from: null, to: '/index'},
//     ]
//   });
const store =new vuex.Store({
    state:{
        count:0,
        pagename:""
    },
    mutations:{
        changepage(state,newname){
            state.pagename = newname
        },
        increment(state) {
            state.count++
        },
        decrease (state) {
            state.count--
        }
    }
})
const routers = [
    { name: '/directory', meta: { title: "openlayers示例" }, path: '/directory', component: Directory },
    { name: '/code', meta: { title: "代码" }, path: '/code', component: (resolve)=>require(["./code.vue"],resolve) },
    { name: '/inspect', meta: { title: "巡检模板" }, path: '/inspect', component: Inspect },
    ...maprouters

]
const RouterConfig = {
    mode: 'history',
    // base:'/test/',
    routes: routers
}
const router = new VueRouter(RouterConfig);
new Vue({
    el: '#app',
    router,
    store,
    render: h =>{
        return  h(App)
    }
});
router.beforeEach((to, from, next) => {
    /* 路由发生变化修改页面title */
    if (to.meta.title) {
      document.title = to.meta.title
    }
    next()
  })
