var webpack = require('webpack');
var VueLoaderPlugin = require('vue-loader/lib/plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var merge = require('webpack-merge');
var webpackBaseConfig = require('./webpack.config.js');
var path = require('path');


webpackBaseConfig.plugins=[];


module.exports = merge(webpackBaseConfig,{
    output: {
        publicPath:'/static/dist/',
        filename: '[name].js',
        chunkFilename:'chunk.[chunkhash].js'
    },
    plugins:[
        new VueLoaderPlugin(),
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV':'"production"',
                'API_URL': "/"+'"' + JSON.stringify(process.env.API_URL) +'/"'
              }
        }),
        new ExtractTextPlugin({
            filename:'[name].css',
            allChunks: true
        }),
        new HtmlWebpackPlugin({
            template:'./index.ejs',
            API_URL_TEMPLATE_VAR: '<%= process.env.API_URL %>',
            filename: '../index.html',
            inject:false
        })
        
    ]
})