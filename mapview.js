var fs = require('fs');
var path = require('path');
var pagePath = path.join(__dirname, './views');
var imports = [];
var routesNames = [];
getRoutes(pagePath);
var fileString = "const maprouters = [\n";
routesNames.forEach(function(item,index){
    fileString+="{\n";

    fileString+="path:'/"+item.name+"',\n";
    fileString+="name:'"+item.name+"',\n";
    fileString+="component:(resolve) => require(['"+item.route+"'],resolve),\n";
    fileString+="},\n"
})
fileString+="{\n";

fileString+="path:'*',\n";
fileString+="redirect:'/directory',\n";
fileString+="},\n"
fileString+="{\n";

fileString+="path:'/',\n";
fileString+="redirect:'/directory',\n";
fileString+="}\n"
fileString += "]\n"
fileString+="export default maprouters"
fs.writeFileSync(path.join(__dirname, './route.js'), fileString);

function getRoutes(filePath, fileName, modulesName) {
    if (!modulesName) {
        modulesName = fileName;
    }

    var stat = fs.statSync(filePath);
    var isDir = stat.isDirectory();
    if (isDir) {
        var files = fs.readdirSync(filePath)
        if (files && files.length) {
            files.forEach(function (fn, index) {
                var fp = path.join(filePath, fn);
                getRoutes(fp, fn, modulesName);
            });
        }
    } else {
        if (fileName) {
            console.log(filePath)
            console.log(pagePath)
            var pathName = filePath.replace(pagePath, '');
            var routesPath = './views'+pathName;
            if (process.platform.indexOf('win') >= 0) {
                routesPath = routesPath.replace(/\\/g, "\/");
            }
            pathName = pathName.replace('.vue', '');
            pathName = pathName.replace('\\', '');
            console.log(pathName.split('\\'))
            routesNames.push({name:pathName.split('\\').join("."),route:routesPath});
        }
    }
}