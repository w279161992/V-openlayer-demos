const tableData = [
    {
    demo: 'dash.vue',
    apply: '绘制实虚线，铁路样式线，间隔线',
    details: '运行代码'
  },{
    demo: 'animation.vue',
    apply: '动画漫游。加载OSM的切片图层，指定地点定位（不同动画漫游），旋转地图',
    details: '运行代码'
  }, {
    demo: 'arcgis-tiled.vue',
    apply: '加载arcgis的地图和图像的切片服务，调用Mapserver的服务',
    details: '运行代码'
  }, {
    demo: 'attributions.vue',
    apply: '属性折叠。加载OSM切片图层，检查分辨率控制属性折叠级别。',
    details: '运行代码'
  }, {
    demo: 'bing-maps.vue',
    apply: '列表加载不同图层。动态显示、隐藏地图BingMaps的图层。需自行创建Bing-maps账户秘钥。',
    details: '运行代码'
  }, {
    demo: 'canvas-tiles.vue',
    apply: '使用HTML5画布在客户端上生成黑色网格图块（类似于添加渔网）。加载OSM切片图层，TileDebug并不从服务器获取数据，而是为切片渲染一个网格。tileGrid: osmSource.getTileGrid()加载切片网格。',
    details: '运行代码'
  }, {
    demo: 'cartodb.vue',
    apply: '地图数据库（不同SQL语句筛选出不同范围）。加载OSM切片图层和地图数据库数据源的切片图层的图层组，根据SQL语句与数据库的交互筛选出合适的图层范围。',
    details: '运行代码'
  }, {
    demo: 'chaikin.vue',
    apply: '绘图平滑处理（需要安装(npm install )chaikin-smooth的npm包）。创建OSM的切片图层作为底图和手绘的矢量图层的图层组。仅绘制线串。',
    details: '运行代码'
  }, {
    demo: 'custom-interactions.vue',
    apply: '自定义交互（创建对象完成鼠标移动要素）。加载TileJSON的切片底图和创建的点线面要素矢量图层作为图层组。定义矢量图层的样式。',
    details: '运行代码'
  }, {
    demo: 'cluster.vue',
    apply: '聚类点要素（点要素为模拟的数据）。OSM的切片底图，模拟矢量点数据作为图层组，加载滑块控件控制集落距离。',
    details: '运行代码'
  }, {
    demo: 'd3.vue',
    apply: '加载TopoJSON几何。加载Stamen的切片图层。加载TopoJSON几何，并使用d3（d3.geo.path）将这些几何渲染为canvas元素，然后将其用作OpenLayers图像图层的图像。',
    details: '运行代码'
  }, {
    demo: 'drag-and-drop-image-vector.vue',
    apply: '拖动GPX，GeoJSON，IGC，KML或TopoJSON文件加载至地图作为矢量图层。添加文件中的要素至地图，移动鼠标查询要素属性。',
    details: '运行代码'
  }, {
    demo: 'drag-and-drop.vue',
    apply: '拖动GPX，GeoJSON，IGC，KML或TopoJSON文件加载至地图作为矢量图层。添加文件中的要素至地图，移动鼠标查询要素属性。没有投影变换支持，因此这仅适用于EPSG：4326和EPSG：3857中的数据。',
    details: '运行代码'
  }, {
    demo: 'device-orientation.vue',
    apply: '跟踪设备方向的更改。加载OSM的切片图层，加载方向控件，随设备方向改变，方向参数也随之改变。',
    details: '运行代码'
  }, {
    demo: 'draw-and-modify-features.vue',
    apply: '绘制修改点、线串、多边形和圆。加载OSM的切片图层，绘制修改点、线串、多边形、圆作为矢量图层加载。',
    details: '运行代码'
  }, {
    demo: 'drag-rotate-and-zoom.vue',
    apply: 'Shift+Drag 围绕其中心旋转和缩放地图。加载OSM切片底图。',
    details: '运行代码'
  }, {
    demo: 'draw-features.vue',
    apply: '从上面的下拉列表中选择几何类型以开始绘制（没有修改功能）。要完成绘图，请单击最后一个点。要激活线条，多边形和圆形的徒手绘图，请按住Shift键。加载OSM切片底图。加载绘制结果作为矢量图层。',
    details: '运行代码'
  }, {
    demo: 'draw-freehand.vue',
    apply: '从上面的下拉列表中选择几何类型以开始徒手绘制（没有修改功能）。ol/interaction/Draw徒手模式。在徒手绘制期间，在拖动时添加点。设置freehand: true为启用徒手模式。加载OSM切片底图。加载绘制结果作为矢量图层。',
    details: '运行代码'
  }, {
    demo: 'draw-shapes.vue',
    apply: '从上面的下拉列表中选择一种形状类型以开始绘图。要激活徒手画，请按住Shift键。加载OSM切片底图。加载绘制结果作为矢量图层。',
    details: '运行代码'
  }, {
    demo: 'earthquake-custom-symbol.vue',
    apply: '',
    details: '运行代码'
  }, {
    demo: 'dynamic-data.vue',
    apply: '模拟数据进行动态旋转。加载OSM切片底图。模拟数据作为矢量数据旋转渲染在地图。',
    details: '运行代码'
  }, {
    demo: 'epsg-4326.vue',
    apply: '在EPSG中创建地图：4326。加载WMS切片底图，比例尺控件，地图坐标系为EPSG:4326。',
    details: '运行代码'
  }, {
    demo: 'export-map.vue',
    apply: '将地图导出为PNG图像。加载OSM切片图层作为地图，加载GeoJSON文件作为矢量图层的图层组。',
    details: '运行代码'
  }, {
    demo: 'extent-interaction.vue ',
    apply: '使用Extent交互来绘制可修改的范围。加载OSM切片图层作为底图，加载GeoJSON文件作为矢量图层。使用Shift+Drag绘制的程度。 Shift+Drag在角度或边缘上调整大小。Shift+Click除去它的程度。',
    details: '运行代码'
  }, {
    demo: 'feature-move-animation.vue',
    apply: '模拟路线行走动画。使用编码折线，利用postcompose和vectorContext沿着路线画一个（标记）的功能。创建矢量图层，矢量要素为路线要素，地质标志，开始标志和结束标志。加载BingMaps切片地图作为底图，加载上述矢量图层。速度控件和开始动画的控件。',
    details: '运行代码'
  }, {
    demo: 'flight-animation.vue',
    apply: '动画航班。加载Stamen的切片底图，加载JSON文件的矢量点数据，用大圆弧形计算公式是arc.js计算出飞行路线，模拟航班飞行。',
    details: '运行代码'
  }, {
    demo: 'feature-animation.vue',
    apply: '动画功能。每次将特征添加到图层时，选择执行flash动画。加载OSM的切片图层作为底图，加载随机点数据作为矢量图层，动画渲染矢量点数据的加载。',
    details: '运行代码'
  }, {
    demo: 'arcgis-image.vue',
    apply: '加载MapServer服务。使用动态ArcGIS REST MapService。加载OSM的切片图层作为底图，ImageArcGISRest服务作为图像图层的图层组。',
    details: '运行代码'
  }, {
    demo: 'geojson-vt.vue',
    apply: '与geojson-vt库集成，加载GeoJSON文件转化成矢量切片图层。加载OSM的切片图层，加载GeoJSON文件转化成矢量切片图层。',
    details: '运行代码'
  }, {
    demo: 'geojson.vue',
    apply: 'GeoJSON功能，自定义要素作为geojsonObject对象加载为矢量图层，加载OSM切片底图。',
    details: '运行代码'
  }, {
    demo: 'geolocation.vue',
    apply: '地理定位。加载OSM的切片底图，获取当前位置绘制坐标点加载为矢量图层。',
    details: '运行代码'
  }, {
    demo: 'getfeatureinfo-image.vue',
    apply: '单击WMS图像层时触发WMS GetFeatureInfo请求，获取要素信息。加载ImageWMS服务作为wms图层，单击要素显示要素属性信息，鼠标样式随鼠标移动设置隐藏。',
    details: '运行代码'
  }, {
    demo: 'getfeatureinfo-tile.vue',
    apply: '单击WMS切片图层时触发WMS GetFeatureInfo请求，获取要素信息。加载TileWMS服务作为wms图层，单击要素显示要素属性信息，鼠标样式随鼠标移动设置隐藏。',
    details: '运行代码'
  }, {
    demo: 'gpx.vue',
    apply: '使用GPX源加载为矢量图层，获取要素信息。加载BingMaps的切片底图，GPX文件转化的矢量图层，鼠标移动和鼠标点击显示要素信息，鼠标样式随鼠标移动进行改变。',
    details: '运行代码'
  }, {
    demo: 'getfeatureinfo-layers.vue',
    apply: '单个WMS GetFeatureInfo请求返回的功能要求按层名读取多个图层。加载xml文件，获取文件中图层个数。',
    details: '运行代码'
  }, {
    demo: 'graticule.vue',
    apply: '向地图添加经纬网覆盖。加载OSM的切片图层，定义经纬网的划线样式添加地图的经纬网。',
    details: '运行代码'
  }, {
    demo: 'heatmap-earthquakes.vue',
    apply: '动态热力图。解析KML文件并将要素呈现为ol/layer/Heatmap矢量图层，加载Stamen切片图层，添加控制热力图模糊度和半径的滑块控制热力图效果。',
    details: '运行代码'
  }, {
    demo: 'hit-tolerance.vue',
    apply: '计算像素的某个距离内的要素个数。加载OSM切片图层，加载指定数组绘制的线串矢量图层，添加控制命中容限的控件，计算命中要素的个数及相应样式的改变。',
    details: '运行代码'
  }, {
    demo: 'icon-negative.vue',
    apply: '使用图标来表示点的示例。单击图标将其选中，然后使用其负图像进行渲染。加载Stamen切片地图，加载图像要素的矢量图层。鼠标移动覆盖要素改变鼠标样式，点击要素图像渲染出负图像。',
    details: '运行代码'
  }, {
    demo: 'here-maps.vue',
    apply: '加载HERE Map切片图层。添加选择图层的控件，根据图层列表选择加载图层。',
    details: '运行代码'
  }, {
    demo: 'icon-sprite-webgl.vue',
    apply: '精灵图像用于图标样式。使用sprite需要使用WebGL获得良好的性能。模拟产生随机数量的要素，更改要素为精灵图像，获取要素点击弹出属性，鼠标移动覆盖要素改变鼠标样式。',
    details: '运行代码'
  }, {
    demo: 'igc.vue',
    apply: '时间滑块进行轨迹回放。加载igc文件转化成矢量图层，加载opencyclemap和thunderforest叠加的图层作为底图，移动鼠标进行最近捕捉点选取及要素属性的展示，加载时间滑块进行轨迹回放。',
    details: '运行代码'
  }, {
    demo: 'image-filter.vue',
    apply: '将滤镜应用于图像，图像数据添加卷积运算列表显示。加载BingMaps地图，运用归一化方法和卷积运算计算 图像数据不同归一化方法下的卷积，添加列表控件，分层控制图像的显示 。',
    details: '运行代码'
  }, {
    demo: 'image-vector-layer.vue',
    apply: '鼠标移动显示要素信息，突出要素的效果。加载geojson文件转化的矢量图层，移动鼠标选择要素加强效果显示，要素属性信息显示。',
    details: '运行代码'
  }, {
    demo: 'KML.vue',
    apply: '解析KML来使用矢量源进行渲染显示要素信息。加载BingMaps切片图层，解析KML文件为矢量图层，鼠标移动改变样式，获取要素属性。',
    details: '运行代码'
  }, {
    demo: 'jsts.vue',
    apply: '显示JSTS与OpenLayers 集成。JSTS为Web制图应用程序提供一个完整的库，用于处理和分析简单的几何图形，但JSTS也可以用作独立的几何库。加载OSM切片底图和geojson文件转换的矢量图层。',
    details: '运行代码'
  }, {
    demo: 'interaction-options.vue',
    apply: '限制分辨率和聚焦的交互。加载OSM切片图层，默认交互作用是限制分辨率和聚焦作用，点击视图才可进行视图缩放。',
    details: '运行代码'
  }, {
    demo: 'layer-extent.vue',
    apply: '修改叠加层的范围。加载不同JSON切片图层作为底图和覆盖层，使用样例范围控件来限制基于范围（近似国家边界）的渲染。',
    details: '运行代码'
  }, {
    demo: 'geolocation-orientation.vue',
    apply: '具有方向的移动定位地理跟踪。加载OSM切片地图，加载json文件数据模拟设备移动，添加地理定位的控件和模拟移动的控件，在html标签页中添加设备的信息。',
    details: '运行代码'
  }, {
    demo: 'layer-clipping-webgl.vue',
    apply: '使用WebGL剪裁图层。加载OSM切片图层。',
    details: '运行代码'
  }, {
    demo: 'layer-spy.vue',
    apply: '围绕最近的鼠标位置设置剪贴蒙版，提供一个图层的望远镜效果。加载BingMaps地图的图层组，在地图上移动以查看望远镜效果。使用↑向上和↓向下箭头键调整望远镜尺寸。',
    details: '运行代码'
  }, {
    demo: 'layer-z-index.vue',
    apply: 'Z-index确定渲染顺序。有两个托管图层（正方形和三角形）和一个非托管图层（星形）。渲染顺序为顶部的三角形，方形和星形。自定义要素为不同图层，添加图层的渲染顺序。',
    details: '运行代码'
  }, {
    demo: 'line-arrows.vue',
    apply: '每个线串段绘制箭头。加载OSM切片底图。绘制线串作为矢量图层，线串加箭头表示绘制方向。',
    details: '运行代码'
  }, {
    demo: 'layer-swipe.vue',
    apply: '图层滑动贴图。加载OSM切片图层，加载BingMaps切片图层，添加滑块控件控制贴图加载。',
    details: '运行代码'
  }, {
    demo: 'localized-openstreetmap.vue',
    apply: '基底层是OpenCycleMap与来自覆盖OpenSeaMap。',
    details: '运行代码'
  }, {
    demo: 'magnify.vue',
    apply: '放大局部区域。使用postcompose事件侦听器对指针位置周围的圆圈中的图像进行过采样。加载BingMaps的切片图层数据源，添加圆圈进行采样放大。',
    details: '运行代码'
  }, {
    demo: 'mapguide-untiled.vue',
    apply: '',
    details: '运行代码'
  }, {
    demo: 'modify-features.vue',
    apply: '修改和选择交互。放大感兴趣的区域并选择要编辑的功能。加载OSM的切片图层和geojson文件转化的矢量图层组，在编辑之前选择多个要素来保留拓扑（Shift+Click以选择多个要素）。',
    details: '运行代码'
  }, {
    demo: 'min-max-resolution.vue',
    apply: '控制最小最大分辨率进行缩放。加载OSM的切片图层，加载TileJSON切片图层。',
    details: '运行代码'
  }, {
    demo: 'mobile-full-screen.vue',
    apply: '手机全屏显示。加载BingMaps地图的切片图层。',
    details: '运行代码'
  }, {
    demo: 'min-zoom.vue',
    apply: '查看最小缩放。加载OSM的切片图层。',
    details: '运行代码'
  }, {
    demo: 'moveend.vue',
    apply: '地图移动获取坐标。加载OSM的切片图层。',
    details: '运行代码'
  }, {
    demo: 'mouse-position.vue',
    apply: '鼠标位置控制。加载OSM切片图层，添加坐标系控制和精确度控制的控件，鼠标移动显示当前坐标。',
    details: '运行代码'
  }, {
    demo: 'overviewmap.vue',
    apply: '鹰眼视图控件的加载。加载OSM的切片图层。',
    details: '运行代码'
  }, {
    demo: 'permalink.vue',
    apply: '创建永久连接。加载OSM的切片图层，window出现问题记录视图状态，包括视图的中心点、初始缩放级别和旋转角度，添加window的popstate突然出现问题事件。',
    details: '运行代码'
  }, {
    demo: 'polygon-styles.vue',
    apply: '自定义多边形样式。自定义多边形正方形和三角形作为矢量图层加载。',
    details: '运行代码'
  }, {
    demo: 'pinch-zoom.vue',
    apply: '缩放约束为整数缩放级别。加载OSM的切片图层，constrainResolution: true在构建交互时进行设置，缩放约束为整数缩放级别。',
    details: '运行代码'
  }, {
    demo: 'navigation-controls.vue',
    apply: 'ZoomToExtent导航控件。加载OSM切片图层，设置默认控件为ZoomToExtent缩放至指定范围。',
    details: '运行代码'
  }, {
    demo: 'reprojection-by-code.vue',
    apply: '显示从OSM（EPSG：3857）到任意投影的客户端光栅重投影功能。加载OSM切片图层，添加输入坐标系输入框、搜索按钮、结果显示、呈现边缘复选框的控件。',
    details: '运行代码'
  }, {
    demo: 'render-geometry.vue',
    apply: '将几何图形渲染到任意画布。绘制指定线串、面、点几何为矢量内容渲染到指定画布。',
    details: '运行代码'
  }, {
    demo: 'regularshape.vue',
    apply: '模拟要素绘制常规形状，绘制方形、三角形、五角星、交叉号、×号作为矢量图层加载。',
    details: '运行代码'
  }, {
    demo: 'reprojection.vue',
    apply: '显示各种投影之间的客户端光栅重投影。添加基础底图列表【OSM (EPSG:3857)和WMS (EPSG:4326)】，加载叠加底图列表【British National Grid (EPSG:27700)、Swisstopo WMS (EPSG:21781)、NASA Arctic WMTS (EPSG:3413)、Grand Canyon HiDPI (EPSG:3857)、United States (EPSG:3857)】，加载投影坐标系列表【Spherical Mercator (EPSG:3857)、WGS 84 (EPSG:4326)、Mollweide (ESRI:54009)、British National Grid (EPSG:27700)、ED50 / UTM zone 32N (EPSG:23032)、US National Atlas Equal Area (EPSG:2163)、NSIDC Polar Stereographic North (EPSG:3413)、RSRGD2000 / MSLC2000 (EPSG:5479)】，添加边缘复选框的控件。',
    details: '运行代码'
  }, {
    demo: 'reprojection-image.vue',
    apply: '显示单个图像源的客户端重新投影。加载OSM的切片图层，加载坐标系为EPSG:27700的Static的图像图层，坐标系为EPSG:27700的图像图层投影到OSM坐标系为EPSG:3857的地图上。',
    details: '运行代码'
  }, {
    demo: 'reprojection-wgs84.vue',
    apply: 'WGS84中OpenStreetMap的客户端重新投影。加载OSM切片图层，将坐标系EPSG:3857重新投影为EPSG:4326。',
    details: '运行代码'
  }, {
    demo: 'reusable-source.vue',
    apply: '更新url更改切片数据源。添加代表不同url的按钮点击添加url为切片图层。',
    details: '运行代码'
  }, {
    demo: 'select-features.vue',
    apply: '定义不同方式选择要素。加载OSM切片图层，加载geojson文件转化的矢量图层，添加点击、单击、徘徊、Alt+点击列表方式进行要素选择。',
    details: '运行代码'
  }, {
    demo: 'scaleline-indiana-east.vue',
    apply: '显示OpenStreetMap的客户端重新投影到NAD83 Indiana East，包括带有美国单位的ScaleLine控件。',
    details: '运行代码'
  }, {
    demo: 'scale-line.vue',
    apply: '添加比例尺线的控件，添加选择比例尺单位的列表控件。加载OSM切片图层。',
    details: '运行代码'
  }, {
    demo: 'rotation.vue',
    apply: '使用Alt+Shift+Drag旋转地图。加载OSM切片图层，添加视图的旋转角度属性。',
    details: '运行代码'
  }, {
    demo: 'simple.vue',
    apply: '简单地图，加载OSM切片图层。',
    details: '运行代码'
  }, {
    demo: 'semi-transparent-layer.vue',
    apply: '半透明图层。加载OSM切片图层，加载平铺的MaxBox切片图层，设置透明度。',
    details: '运行代码'
  }, {
    demo: 'snap.vue',
    apply: '绘制和修改要素（点线面圆）。加载OSM切片图层，加载绘制的矢量图层，绘制和修改要素，加载选择和修改勾选项和几何类型列表选择的控件。',
    details: '运行代码'
  }, {
    demo: 'static-image.vue',
    apply: '使用静态图像作为图层源。地图视图配置有自定义投影，可将图像坐标直接转换为地图坐标。加载Static的图像图层。',
    details: '运行代码'
  }, {
    demo: 'sphere-mollweide.vue',
    apply: '带有经纬网组件的球体Mollweide地图。加载geojson文件转化的矢量数据源，创建矢量图层球面Mollweide。为球面Mollweide投影对象配置一个范围和一个世界范围，添加经纬网。',
    details: '运行代码'
  }, {
    demo: 'stamen.vue',
    apply: '带有地形标签的水彩基础图层Stamen的切片图层组。',
    details: '运行代码'
  }, {
    demo: 'symbol-atlas-webgl.vue',
    apply: '提供AtlasManager地图集管理器加载符号样式的地图集。自定义圆和星星符号样式，模拟数量加载为矢量图层。',
    details: '运行代码'
  }, {
    demo: 'synthetic-lines.vue',
    apply: '合成线实例。绘制多条线合成面。模拟一定数量的线，加载为矢量图层。',
    details: '运行代码'
  }, {
    demo: 'synthetic-points.vue',
    apply: '合成点实例。模拟一定数量的点，加载为矢量图层。添加鼠标移动位置改变鼠标样式事件，显示要素捕捉事件。',
    details: '运行代码'
  }, {
    demo: 'street-labels.vue',
    apply: '沿路径添加街道名称。加载BingMaps的切片图层，加载geojson文件转化的矢量图层，使用文本样式placement：‘line’沿路径呈现文本。',
    details: '运行代码'
  }, {
    demo: 'teleport.vue',
    apply: '将地图从一个目标移动到另一个目标。加载OSM的切片图层，添加传送按钮。',
    details: '运行代码'
  }, {
    demo: 'tilejson.vue',
    apply: 'TileJSON图层。加载TileJSON的切片图层。',
    details: '运行代码'
  }, {
    demo: 'tile-transitions.vue',
    apply: '使用不透明度过渡渲染切片。加载XYZ数据源设置可见性，建立过渡和非过渡图层，添加使用不透明度过渡渲染复选框。',
    details: '运行代码'
  }, {
    demo: 'topolis.vue',
    apply: '创建和编辑拓扑几何。标准交互绘制边缘，捕捉到现有边缘。通过绘制与要删除的边相交的新边来删除边。加载OSM切片图层和绘制的节点图层、边缘图层、面域图层的矢量图层。',
    details: '运行代码'
  }, {
    demo: 'tissot.vue',
    apply: '天梭印迹图。顶部的是EPSG:4326地图。底部的那个是EPSG:3857地图。加载TileWMS服务的切片图层，自定义圆要素添加至矢量图层。',
    details: '运行代码'
  }, {
    demo: 'translate-features.vue',
    apply: '选择移动要素。加载OSM切片图层，加载geojson文件转化的矢量图层，选择要素移动要素。',
    details: '运行代码'
  }, {
    demo: 'turf.vue',
    apply: '沿街道每200米显示一个标记。加载OSM切片图层，加载geojson文件转化的矢量图层和标记点生成的矢量图层。',
    details: '运行代码'
  }, {
    demo: 'vector-esri.vue',
    apply: 'ArcGIS REST功能服务。加载ArcGIS REST功能服务的要素服务的矢量图层，加载ArcGIS REST功能服务的地图服务的栅格图层，鼠标移动获取要素信息和改变鼠标样式。',
    details: '运行代码'
  }, {
    demo: 'vector-label-decluttering.vue',
    apply: '矢量标签整理。整理用于避免overflow: true在文本样式上设置重叠标签。对于MultiPolygon几何图形，仅在自定义geometry函数中选择最宽的多边形。加载geojson文件转化的矢量图层，设置图层和标签样式。',
    details: '运行代码'
  }, {
    demo: 'vector-layer.vue',
    apply: '加载矢量图层。加载geojson文件转化的矢量图层，有关国家/地区的信息会在悬停或单击时显示。',
    details: '运行代码'
  }, {
    demo: 'vector-osm.vue',
    apply: '加载OSM XML矢量数据。加载BingMaps切片图层，加载OSM XML矢量图层。',
    details: '运行代码'
  }, {
    demo: 'vector-esri-edit.vue',
    apply: '从ArcGIS REST Feature Service加载功能，并允许添加面要素或更改面要素。加载ArcGIS REST功能服务的要素服务的矢量图层，加载ArcGIS REST功能服务的地图服务的栅格图层，添加绘制与修改列表控件，添加面要素或更改面要素。',
    details: '运行代码'
  }, {
    demo: 'vector-tile-selection.vue',
    apply: '单击渲染的矢量切片要素以在地图上突出显示它，单击空白点（海洋）以重置选择。通过将操作类型更改为“多选”，您可以一次选择多个功能。加载MVT的矢量切片图层。',
    details: '运行代码'
  }, {
    demo: 'vector-wfs.vue',
    apply: '从GeoServer WFS加载新功能。加载GeoJSON格式的矢量图层，加载BingMaps的切片图层。',
    details: '运行代码'
  }, {
    demo: 'vector-wfs-getfeature.vue',
    apply: '发布请求以加载与查询匹配的功能。加载BingMaps切片图层，使用a PropertyIsEqualTo和PropertyIsLike过滤器的请求，然后发布请求并将接收到的要素添加到矢量图层中。',
    details: '运行代码'
  }, {
    demo: 'topojson.vue',
    apply: '使用矢量图层ol/format/TopoJSON来渲染TopoJSON中的要素。加载TileJSON切片图层，加载json文件转化的TopoJSON格式的矢量图层。',
    details: '运行代码'
  }, {
    demo: 'wkt.vue',
    apply: '从WKT（标准文本）格式的几何创建面要素。加载OSM的切片图层，从WKT（标准文本）格式的几何创建面要素为矢量图层。',
    details: '运行代码'
  }, {
    demo: 'wms-custom-proj.vue',
    apply: '将自定义坐标转换函数添加到已配置的投影。加载TileWMS的WMS切片服务图层组，添加比例尺线控件。',
    details: '运行代码'
  }, {
    demo: 'wms-custom-tilegrid-512x256.vue',
    apply: '使用具有非方形切片的自定义切片网格。加载OSM数据源和WMS切片服务的切片图层组，自定义切片网格作为WMS切片服务属性。',
    details: '运行代码'
  }, {
    demo: 'preload.vue',
    apply: '预加载切片。顶部的地图预加载低分辨率图块。底部的地图不使用任何预加载。尝试缩小和平移以查看差异。加载BingMaps的切片图层。',
    details: '运行代码'
  }, {
    demo: 'wms-no-proj.vue',
    apply: '加载没有投影的WMS服务。使用code和units配置的投影，加载TileWMS的WMS切片服务的切片图层，加载ImageWMS的WMS图像服务的图像图层。',
    details: '运行代码'
  }, {
    demo: 'wms-image-custom-proj.vue',
    apply: '在任意投影之间转换坐标。加载WMS图像服务的图像图层组，定义投影坐标系EPSG:21781，添加比例尺线控件。',
    details: '运行代码'
  }, {
    demo: 'wms-image.vue',
    apply: '加载WMS图像服务。加载OSM的切片图层和WMS图像图层。（WMS可用作图像层，或用作图块层，如Tiled WMS示例示例中所示。可以缓存切片，因此浏览器不会重新获取已经查看过的区域的数据。但是，对于不了解切片的WMS服务器的重复标签可能存在问题，在这种情况下，单个图像WMS将产生更好的制图。）',
    details: '运行代码'
  }, {
    demo: 'wms-tiled.vue',
    apply: '加载TileWMS服务。加载OSM的切片图层和WMS切片图层。',
    details: '运行代码'
  }, {
    demo: 'wmts.vue',
    apply: '加载WMTS服务。加载OSM和WMTS切片图层组。需自行定义投影坐标系。',
    details: '运行代码'
  }, {
    demo: 'wms-tiled-wrap-180.vue',
    apply: '加载在180°子午线上WMS切片服务。加载OSM的切片图层和WMS切片图层。',
    details: '运行代码'
  }, {
    demo: 'wms-time.vue',
    apply: '在连续更改时间维度时，演示图层的平滑重新加载。加载Stamen的切片图层和TileWMS的服务切片，添加动画演示和停止的按钮。',
    details: '运行代码'
  }, {
    demo: 'wmts-dimensions.vue',
    apply: '在连续更改尺寸时，演示图层的平滑重新加载。加载OSM切片图层和WMTS数据源的切片图层组，添加维度滑块。',
    details: '运行代码'
  }, {
    demo: 'wmts-ign.vue',
    apply: '显示IGN WMTS层。创建新的WMTS服务加载为切片图层。',
    details: '运行代码'
  }, {
    demo: 'xyz.vue',
    apply: '加载XYZ数据源的切片图层。XYZ源用于通过包含缩放级别和平铺网格x / y坐标的URL访问的切片数据。',
    details: '运行代码'
  }, {
    demo: 'wmts-layer-from-capabilities.vue',
    apply: '创建用于从GetCapabilities响应访问WMTS的配置。加载OSM的切片图层，加载WMTS数据源的切片图层。',
    details: '运行代码'
  }, {
    demo: 'xyz-esri.vue',
    apply: '支持ArcGIS REST切片服务ol/source/XYZ。加载XYZ数据源的切片图层，加载arcgis的MapServer。',
    details: '运行代码'
  }, {
    demo: 'zoom-constrained.vue',
    apply: '约束缩放。加载BingMaps的切片图层，设置实体图与最大最小缩放级别，这是使用minZoom和maxZoom视图选项完成的。',
    details: '运行代码'
  }, {
    demo: 'xyz-retina.vue',
    apply: '使用MapTiler从GeoTIFF文件准备切片。加载OSM切片图层和XYZ数据源的切片图层组，XYZ源接受tilePixelRatio选项。',
    details: '运行代码'
  }, {
    demo: 'xyz-esri-4326-512.vue',
    apply: '支持具有自定义切片大小和投影的ArcGIS REST切片服务ol/source/XYZ。自定义切片url功能用于处理缩放级别偏移。加载XYZ数据源的切片图层。',
    details: '运行代码'
  }, {
    demo: 'zoomify.vue',
    apply: '将Zoomify源与像素投影一起使用。Zoomify是一种用于深度缩放到高分辨率图像的格式。还处理具有JTL扩展的因特网成像协议（IIP）。添加控制图层改变的列表控件。',
    details: '运行代码'
  }, {
    demo: 'blend-modes.vue',
    apply: '在post-和precompose事件处理程序中更改画布合成/混合模式。可以更改颜色在地图上组合时的效果。可以在选择字段中选择操作，还可以通过图层复选框控制所选操作将影响哪些图层。添加blend-modes的列表控件，画布合成/混合模式复选框控件。',
    details: '运行代码'
  }, {
    demo: 'accessible.vue',
    apply: '无障碍地图。加载OSM的切片图层，添加放大、缩小的控件。',
    details: '运行代码'
  }, {
    demo: 'button-title.vue',
    apply: '使用Bootstrap自定义按钮工具提示。加载OSM的切片图层，为内置的缩放控件与旋转控件添加tooltip提示信息。',
    details: '运行代码'
  }, {
    demo: 'center.vue',
    apply: '调整地图的视图，使几何或坐标位于特定像素位置。上面的地图在视口内应用了顶部，右侧，底部和左侧填充。视图的fit方法用于使用相同的填充来拟合视图中的几何。使用Alt+Shift+Drag旋转地图。加载geojson文件转化为GeoJSON格式的矢量图层，加载OSM切片图层，添加放大到瑞士 （最合适），放大到瑞士 （尊重决议限制）。放大到瑞士 （最近的），放大到洛桑 （最低分辨率），洛桑中心的控件控制视图。',
    details: '运行代码'
  }, {
    demo: 'custom-controls.vue',
    apply: '自定义创建向北旋转的按钮。加载OSM的切片图层，添加向北旋转的按钮。',
    details: '运行代码'
  }, {
    demo: 'box-selection.vue',
    apply: '拉框选择要素并显示要素信息。加载OSM切片图层和geojson文件转化为GeoJSON格式的矢量图层，拉框选择要素并显示要素信息。',
    details: '运行代码'
  }, {
    demo: 'color-manipulation.vue',
    apply: '颜色处理。光栅源允许任意操纵像素值。输入图块源上的RGB值在使用第二个光栅源渲染之前以像素方式操作进行调整。光栅操作将RGB空间中的像素转换为HCL颜色空间，根据色调、浓度、亮度控件调整值，然后将它们转换回RGB空间进行渲染。加载Stamen数据源转化的栅格数据加载为图像图层，添加色调、浓度、亮度控件控制栅格图层。',
    details: '运行代码'
  }, {
    demo: 'earthquake-clusters.vue',
    apply: '地震集群。解析KML文件并将这些要素渲染为矢量图层上的聚类。加载Stamen数据源的栅格图层和解析KML文件加载为矢量图层，将要素渲染为矢量图层上的聚类。',
    details: '运行代码'
  }, {
    demo: 'fractal.vue',
    apply: '渲染具有许多顶点的要素。自定义三角形矢量图层，添加深度输入滑块控件，渲染三角形分形。',
    details: '运行代码'
  }, {
    demo: 'full-screen-source.vue',
    apply: '具有扩展源元素的全屏控制。单击右上角的控件以全屏显示。再次单击它以退出全屏。如果地图上没有按钮，则您的浏览器不支持全屏API。加载OSM切片图层，添加全图控件和侧面板。',
    details: '运行代码'
  }, {
    demo: 'export-pdf.vue',
    apply: '使用jsPDF库将地图导出为PDF。加载OSM切片图层，以WKT（）格式读取面要素加载为矢量图层，添加页面大小和解析度的列表控件，添加导出PDF按钮控件。',
    details: '运行代码'
  }, {
    demo: 'full-screen-drag-rotate-and-zoom.vue',
    apply: '全屏拖动，旋转和缩放。按住Shift+Drag可旋转和缩放。单击右上角的按钮以全屏显示。然后再做Shift+Drag一次。如果地图上没有按钮，则您的浏览器不支持全屏API。加载BingMaps的切片图层。',
    details: '运行代码'
  }, {
    demo: 'full-screen.vue',
    apply: '全屏控制。单击右上角的控件以全屏显示。再次单击它以退出全屏。如果地图上没有按钮，则您的浏览器不支持全屏API。加载BingMaps的切片图层。',
    details: '运行代码'
  }, {
    demo: 'icon-color.vue',
    apply: '将自定义颜色分配给图标。加载自定义罗马，伦敦，马德里的要素为矢量图层，加载TileJSON数据源的栅格图层，定义矢量图层要素的样式。',
    details: '运行代码'
  }, {
    demo: 'kml-earthquakes.vue',
    apply: '解析KML文件并将要素呈现为矢量图层。该层被赋予一个样式，其呈现具有相对于其大小的尺寸的地震位置。加载Stamen数据源的切片图层，加载解析kml文件转化的矢量数据源的矢量图层，鼠标移动显示要素信息弹出框。',
    details: '运行代码'
  }, {
    demo: 'image-load-events.vue',
    apply: '图像源触发与图像加载相关的事件。在地图底部呈现图像加载进度条。加载WMS图片服务为图片图层。',
    details: '运行代码'
  }, {
    demo: 'icon.vue',
    apply: '使用图标表示点。自定义创建新的要素为图像要素加载为矢量图层，加载TileJSON服务为栅格图层，使用图标表示点，鼠标点击显示要素信息，鼠标覆盖要素改变鼠标样式。',
    details: '运行代码'
  }, {
    demo: 'layer-group.vue',
    apply: '带有图层组的地图。加载OSM切片图层，加载TileJSON的切片图层的图层组，设置图层的能见度复选框和不透明度滑块。',
    details: '运行代码'
  }, {
    demo: 'layer-clipping.vue',
    apply: '图层剪切。加载OSM切片图层，自定义心形裁剪图层，加载图层的预构图和后构图事件。',
    details: '运行代码'
  }, {
    demo: 'kml-timezones.vue',
    apply: 'KML中的时区。解析KML文件并将要素呈现为矢量图层。该图层给出了一个ol/style/Style填充时区黄色的图像，其中不透明度是根据当地中午的当前偏移量计算出来的。加载Stamen数据源的栅格图层和解析KML文件并将要素呈现为矢量图层，移动鼠标显示要素信息弹出框。',
    details: '运行代码'
  }, {
    demo: 'mapbox-vector-tiles.vue',
    apply: '一个简单的矢量切片地图。加载Mapbox是矢量切片数据源为矢量切片图层。',
    details: '运行代码'
  }, {
    demo: 'lazy-source.vue',
    apply: '设置/取消设置图层源。加载OSM切片图层，添加为图层设置数据源和清空数据源的按钮。',
    details: '运行代码'
  }, {
    demo: 'mapbox-vector-tiles-advanced.vue',
    apply: '矢量图块地图，其重复使用相同的源图块以用于后续缩放级别以节省移动设备上的带宽。加载Mapbox是矢量切片数据源为矢量切片图层。',
    details: '运行代码'
  }, {
    demo: 'measure.vue',
    apply: '计算几何长度和面积。加载OSM切片图层为栅格数据。加载绘制要素为矢量图层，计算绘制几何长度和面积。添加测量类型列表控件。',
    details: '运行代码'
  }, {
    demo: 'overlay.vue',
    apply: '点击地图显示弹出窗口及信息。创建新的OSM切片图层。添加维也纳的标记，鼠标点击地图显示属性信息。',
    details: '运行代码'
  }, {
    demo: 'modify-test.vue',
    apply: '测试功能修改。自定义创建geojson对象加载为矢量图层，选择修改要素。',
    details: '运行代码'
  }, {
    demo: 'osm-vector-tiles.vue',
    apply: '一个简单的Mapzen矢量切片地图。使用TopoJSON格式的layerName选项来确定样式的图层（“水”，“道路”，“建筑物”）。加载mapzen地图的矢量切片数据源为矢量切片图层。',
    details: '运行代码'
  }, {
    demo: 'overviewmap-custom.vue',
    apply: '自定义概览图控件。创建OSM的切片图层，加载thunderforest地图。添加概览图控件和拖拽旋转缩放事件。',
    details: '运行代码'
  }, {
    demo: 'region-growing.vue',
    apply: '种子生长蔓延。栅格源接受任意数量的输入源（基于图块或图像），并对输入数据运行操作管道。加载BingMaps地区的切片图层为图像集。根据阈值滑块的输入显示栅格图像。',
    details: '运行代码'
  }, {
    demo: 'popup.vue',
    apply: '单击地图以获得弹出窗口。弹出窗口由几个基本元素组成：容器，关闭按钮和内容的位置。添加TileJSON服务的切片图层，鼠标点击加载弹出窗口及属性信息。',
    details: '运行代码'
  }, {
    demo: 'raster.vue',
    apply: '栅格源接受任意数量的输入源（基于图块或图像）并在输入像素上运行操作管道。对于每个像素，从输入像素计算植被绿度指数（VGI）。加载BingMaps切片图层和根据输入源的输入生成的栅格图像图层。添加控制输入像素的控件。',
    details: '运行代码'
  }, {
    demo: 'sea-level.vue',
    apply: '带有 Mapbox Terrain-RGB图块的“泛洪”海平面滑块上显示的高程下方的区域。创建新的mapbox的地图XYZ服务为海拔高度，加载海拔高度的数据源为图像图层，加载mapbox的地图XYZ服务的切片图层，添加海平面滑块和 旧金山，纽约，孟买或上海的定位。',
    details: '运行代码'
  }, {
    demo: 'shaded-relief.vue',
    apply: '生成给定高程数据的阴影浮雕图像。栅格源接受任意数量的输入源（基于图块或图像），并对输入数据运行操作管道。创建新的mapbox地图的XYZ服务为海拔高度，加载海拔高度为栅格数据，加载OSM切片图层，添加垂直度、太阳高度、太阳方位角的滑块控件，根据滑块输入改变栅格数据。',
    details: '运行代码'
  }, {
    demo: 'side-by-side.vue',
    apply: '共享视图。两个地图（一个使用Canvas渲染器，一个使用WebGL渲染器）共享相同的中心，分辨率，旋转和图层。加载OSM切片图层。',
    details: '运行代码'
  }, {
    demo: 'tile-load-events.vue',
    apply: '加载进度条。图像平铺源会触发与平铺加载相关的事件。加载TileJSON服务的切片图层。',
    details: '运行代码'
  }, {
    demo: 'utfgrid.vue',
    apply: '指向一个国家，看看它的名字和旗帜。加载mapbox的TileJSON服务为切片图层，加载mapbox的UTFGrid服务为切片图层，鼠标移动显示国家信息和国家旗帜，鼠标移动更改鼠标样式。',
    details: '运行代码'
  }, {
    demo: 'wms-capabilities.vue',
    apply: '解析WMS功能响应显示结果对象的内容。读取xml文件获取文本信息。',
    details: '运行代码'
  }, {
    demo: 'vector-tile-info.vue',
    apply: '将指针移到渲染的要素上以显示要素属性。加载arcgis的矢量切片服务为矢量切片图层。鼠标移动显示要素信息。',
    details: '运行代码'
  }, {
    demo: 'wmts-capabilities.vue',
    apply: '解析WMTS功能响应显示结果对象的内容。读取xml文件获取文本信息。',
    details: '运行代码'
  }, {
    demo: 'wmts-hidpi.vue',
    apply: '读取xml文件加载WMTS服务的切片图层，设置tilePixelRatio选项。WMTS源有一个tilePixelRatio选项。支持HiDPI的WMTS可以提供具有512x512像素图块的图块，但是在256x256像素图块网格中使用它们。在这种情况下tilePixelRatio需要设置为2。',
    details: '运行代码'
  }, {
    demo: 'zoomslider.vue',
    apply: '添加缩放滑块。加载OSM切片图层，添加默认样式的缩放滑块，滑块在放大缩小下边。添加放置在缩放控件之间的缩放滑块。添加水平并完全重新设计的缩放滑块。',
    details: '运行代码'
  }, {
    demo: 'vector-labels.vue',
    apply: '在文本样式上设置的许多选项。加载解析geojson文件的矢量图层，加载OSM切片图层，在文本样式上设置的许多选项。',
    details: '运行代码'
  }, {
    demo: 'canvas-gradient-pattern.vue',
    apply: '创建了一个CanvasPattern和一个CanvasGradient。这些国家/地区是从GeoJSON文件加载的。样式函数确定每个国家/地区是否使用CanvasGradient（彩虹色）或CanvasPattern（重复堆叠的圆圈）填充。',
    details: '运行代码'
  }]
  export default tableData